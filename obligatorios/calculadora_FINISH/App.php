<?php

class App
{

    function __construct(){
        session_start();
    }

    public function home(){//metodo que se inicia primero
        $solucion=0;
        require('viewCalculadora.php');
        unset($_SESSION['mensajes']);
    }//fin metodo home

    public function calcular(){//metodo que nos calculara las operaciones

        if (!isset($_REQUEST['numero1'])){
            $_SESSION['mensajes'][]="Operador1 no introducido";
        }else{
            $numero1 = $_REQUEST['numero1'];
        }
        if (!isset($_REQUEST['numero2'])){
            $_SESSION['mensajes'][]="Operador2 no introducido";
        }else{
            $numero2 = $_REQUEST['numero2'];
        }

        if(!is_numeric($numero1)){
            $_SESSION['mensajes'][]="Operador1 mal introducido";
        }
        if(!is_numeric($numero2)){
            $_SESSION['mensajes'][]="Operador2 mal introducido";
        }

        if(isset($_SESSION['mensajes'])){
            header('Location:?method=home');
            return;
        }

        $operaciones = $_REQUEST['operaciones'];
        $solucion=0;

        switch ($operaciones) {
            case '+':
            $solucion = $numero1 + $numero2;
            break;
            case '-':
            $solucion = $numero1 - $numero2;
            break;
            case '*':
            $solucion = $numero1 * $numero2;
            break;
            case '/':
            $solucion = $numero1 / $numero2;
            break;
        }
        require('viewCalculadora.php');
        unset($_SESSION['mensajes']);
    }//fin metodo calcular

}//fin clase App
