
<!DOCTYPE html>
<html>
<head>
    <title>Calculadora </title>
    <meta charset="UTF-8">
</head>
<body>
    <h2>CALCULADORA: </h2>
    <form method="post" action="?method=calcular">
        <label>N1 </label><input type="text" name="numero1">
        <select name="operaciones">
         <optgroup label="OPERADORES">
            <option>+</option>
            <option>-</option>
            <option>*</option>
            <option>/</option>
        </optgroup>
    </select>
    <label>N2 </label><input type="text" name="numero2">
    <input type="submit" value="Calcular">
</form>
<hr>

<h3>La solución es: </h3>
<?php echo $solucion ?>

<?php if (isset($_SESSION['mensajes'])): ?>
    <?php foreach ($_SESSION['mensajes'] as $mensaje): ?>
        <li>
            <?php echo $mensaje ?>
        </li>
    <?php endforeach ?>
<?php endif ?>
<?php unset($_SESSION['mensajes']); ?>
</body>
</html>
