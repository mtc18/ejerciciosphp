<?php

class App{

  function __construct(){
  }

  public function home(){//metodo inicial
    $directorio = opendir("uploads/");
    $arrayFotos = array();
    while ($archivo = readdir($directorio)) {
      if (!is_dir($archivo)) {
        array_push($arrayFotos,$archivo);
      }
    }
    require('viewGaleria.php');
  }//fin metodo home

  public function añadir(){//metodo para subir las imagenes
    $subido = true;
    $msg= ' ';
    $uploadedfile_size = $_FILES['ficheroSubido']['size'];
    echo $_FILES['ficheroSubido']['name'];

    if ($_FILES['ficheroSubido']['size'] > 500000) {
      $msg = $msg. "El archivo es mayor que 500KB, debes reduzcirlo antes de subirlo<br>";
      $subido = false;
    }

    if (!($_FILES['ficheroSubido']['type'] =="image/jpeg" || $_FILES['ficheroSubido']['type'] =="image/gif")) {
      $msg = $msg." Tu archivo tiene que ser JPG o GIF.<br>";
      $subido = false;
    }

    $file_name = $_FILES['ficheroSubido']['name'];
    $add="/var/www/html/ejerciciosphp/obligatorios/galeria/uploads/$file_name";

    if ($subido) {
      if (move_uploaded_file($_FILES['ficheroSubido']['tmp_name'], $add)) {
        echo "Archivo subido correctamente";
      } else {
        $msg = $msg. "Error al subir archivo";
      }
    } else {
      echo $msg;
    }
  }//fin metodo subir

  public function verGrande(){//metodo para ver a su tamaño original las fotos
    var_dump($_POST);
    $pagina=$_POST['verGrande'];
    header("Location: $pagina");
  }//fin metodo verGrande

  public function borrar(){//metodo para borrar fotos de la galeria
    $path="uploads/";
    $file=$_REQUEST['borrar'];
    if (!unlink($file)) {
      echo "Error borrando $file";
    }
    else{
      echo "$file borrado";
    }
  }//fin metodo borrar

}//clase App
