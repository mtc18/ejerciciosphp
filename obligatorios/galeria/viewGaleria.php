<!DOCTYPE html>
<html>
<head>
    <title>Galeria</title>
</head>
<body>
    <h1>Galería Fotográfica:</h1>
    <br>
    <form enctype="multipart/form-data" action="?method=añadir" method="post"><!-- metodo subir fotos-->
        <input name="ficheroSubido" type="file" />
        <input type="submit" value="Subir Imagen" />
    </form><hr><br>

    <?php foreach ($arrayFotos as $imagen): ?>
        <img src="<?php echo "uploads/$imagen" ?>" height="100" width="100" >
        <li><?php echo "$imagen"; ?></li><!-- muestra el nombre de la imagen -->
        <a><br>
            <form method="post" action="?method=verGrande"><!-- metodo ampliar -->
                <input type="hidden" name="verGrande" value="<?php echo "uploads/$imagen" ?>">
                <input type="submit" value="Ampliar">
            </form>
        </a>
        <a>
            <form method="post" action="?method=borrar"><!-- metodo borrar  -->
                <input type="hidden" name="borrar" value="<?php echo "uploads/$imagen" ?>">
                <input type="submit" value="Borrar" name="Borrar"><br>
            </form>
        </a>
        <hr>
    <?php endforeach ?>

    <br>
    <a href="?method=home">Volver a inicio</a>
</body>
</html>
