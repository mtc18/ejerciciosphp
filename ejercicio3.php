<!-- - Crea un array ordenado con los días de la semana.
- Muestra el contenido del array.
- Hazlo de distintas maneras: declaración del array en una sola sentencia, añadiendo elementos uno por uno, ....
- Usa `<ul>...</ul>` o `<table>...</table>` -->

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Ejercicio 3</title>
    </head>
        <body>
            <h1>Array dias de la semana: </h1>
   <?php

$array1= array ("lunes", "martes", "miercoles", "jueves", "viernes", "sabado", "domingo");

$array2[]="lunes";
$array2[]="martes";
$array2[]="miercoles";
$array2[]="jueves";
$array2[]="viernes";
$array2[]="sabado";
$array2[]="domingo";


// //for
// for ($i=0; $i < count($array1); $i++) {
//     echo "<li> $array1 </li>";
// }

//foreach simple con array2
  echo "<hr> Recorrido mediante bucle foreach simple <br>";
foreach ($array1 as $element){
    echo $element . '<br>';
}

//foreach simple con array2
  echo "<hr> Recorrido mediante bucle foreach simple <br>";
foreach ($array2 as $element){
    echo $element . '<br>';
}

//foreach clave->elemento con array1
echo "<hr> Recorrido mediante bucle foreach con posicion->elemento <br>";
foreach ($array1 as $position=>$element){
    echo $position . ": " . $element . '<br>';
}

//foreach clave->elemento con array2
echo "<hr> Recorrido mediante bucle foreach con posicion->elemento <br>";
foreach ($array2 as $position=>$element){
    echo $position . ": " . $element . '<br>';
}

?>
        </body>
</html>
