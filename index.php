<!DOCTYPE html>
<html>
<head>
    <title>Ejercicios php</title>
</head>
<body>
    <h1>Ejercicios PHP </h1>
    <h2>Alumna: Maria Tejel Catalan</h2>
    Crea enlaces a tus ejercicios:
    <ul>
        <li><a href="ejerciciosphp/ejercicio1.php"> Ejercicio 1</a></li>
        <li><a href="ejerciciosphp/ejercicio2.php"> Ejercicio 2</a></li>
        <li><a href="ejerciciosphp/ejercicio3.php"> Ejercicio 3</a></li>
        <li><a href="ejerciciosphp/ejercicio4.php"> Ejercicio 4</a></li>
        <li><a href="ejerciciosphp/ejercicio5.html"> Ejercicios 5 y 6HTML</a></li>
        <li><a href="ejerciciosphp/ejercicio5.php"> Ejercicios 5 y 6 PHP</a></li>
        <li><a href="ejerciciosphp/ejercicio7.php"> Ejercicio 7 PHP</a> </li>
        <li><a href="ejerciciosphp/ejercicio8.php"> Ejercicio 8 PHP</a></li>
        <li><a href="ejerciciosphp/cookies.php"> Cookies</a></li>
        <li><a href="ejerciciosphp/poo/index.php"> Cookies MAS Sesiones (POO1)</a></li>
        <li><a href="ejerciciosphp/poo2/index.php"> Sesiones (POO2)</a></li>
        <li><a href="ejerciciosphp/sesiones/login.php"> Sesiones</a></li>
        <hr>
        <li><a href="/ejerciciosphp/obligatorios/calculadora_FINISH/index.php"> CALCULADORA</a></li>
        <li><a href="/ejerciciosphp/obligatorios/galeria/index.php"> GALERIA</a></li>
        <li><a href="/ejerciciosphp/obligatorios/loteria/index.php"> LOTERIA</a></li>

    </ul>
</body>
</html>
