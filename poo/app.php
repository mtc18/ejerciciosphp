<?php

//CLASE PRINCIPAL

class App
{
    private $atributo;

    function __construct($param) //constructor
    {
        echo "Construyendo mi app <br>";
    //$this->index();
        $this->atributo=$param;
    }

    public function index(){
        echo "En mi metodo index<br>";
        if (isset($_COOKIE['name'])) {
            $name=$_COOKIE['name'];
        }
        require("cookiesvista.php");
    }

    // public function hello(){
    //     echo "helloo";  }

    public function home(){

        if (isset($_REQUEST['name']) && ! empty($_REQUEST['name'])) {

            $name=$_REQUEST['name'];

            if(isset($_REQUEST['remember'])){
                echo "Bienvenido $name ";
                setcookie('name', $name);
                echo "<br> Cookie creada";
            }else{
                setcookie('name', $name, 1);
            }
        }
        require("home.php");
    }
}
