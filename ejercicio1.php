<!--  Crea un fichero PHP donde te presesntes como alumno.
- Tu información (nombre, apellidos, edad, ...) debe estar guardada en variables.
- Hazlo usando echo y print
- Usa el entrecomillado simple y doble para ver las distintas posibilidades.
- Usa etiquetas html para mejorar la presentación. -->

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Ejercicio 1</title>
    </head>
        <body>
            <h1>Informacion: </h1>
   <?php
$nombre = "Maria";
$apellidos= "Tejel Catalan";
$edad= "20 años";
$pueblo= "Sastago";

echo "Hola me llamo $nombre $apellidos tengo $edad  y naci en $pueblo <br>";
print("Hola me llamo $nombre $apellidos tengo $edad y naci en $pueblo");
  ?>
        </body>
</html>
