<!-- - Vamos a calcular el precio con IVA de un producto.
- Carga los datos en variables (precioUnidad, cantidad, iva, precioSinIva, precioConIva, ...)
- Realiza los cálculos pertinentes y muestra detalladamente cada valor inicial y calculado. -->

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Ejercicio 2</title>
    </head>
        <body>
            <h1>IVA de un producto: </h1>
   <?php
$precioUnidad = 10;
$cantidad= 2;
$iva= 0.21;
$precioSinIva= $precioUnidad * $cantidad;
$precioConIva=((($iva * $precioSinIva) + $precioUnidad)* $cantidad);
$ivaCantidad=(($precioConIva - $precioSinIva));

echo "Si compras $cantidad unidades de bollos de leche a $precioUnidad sin iva, con iva es $precioConIva la diferencia de precio es de $ivaCantidad ";
  ?>
        </body>
</html>
