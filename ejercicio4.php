<!-- - Crea un array asociativo que contenga el quinteto inicial de un equipo de baloncesto.
- Muestra su contenido con un bucle foreach.
- Intenta distintas varianttes, accediendo o no a la clave -> valor. -->

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Ejercicio 4</title>
    </head>
        <body>
            <h1>Array asociativo: </h1>
   <?php

echo "Array ordenado inicializado en la declaración <hr>";
$array1 = array (
    'jugador1' => "Maria Tejel",
    'jugador2' => 'Ramiro Tejel',
    'jugador3' => 'Jorge Tejel',
    'jugador4' => "Maite Catalan",
    'jugador5' => "Tony",
);


//foreach clave->elemento1
echo "Recorrido mediante bucle foreach con posicion->elemento <br>";
foreach ($array1 as $position=>$element){
    echo $position . ": " . $element . '<br>';
}
echo 'añadimos un elemento más y hacemos un volcado: <br>';
$array1['jugador6'] = 'Antonio Stark';
var_dump($array1);

?>
        </body>
</html>
