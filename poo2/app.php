<?php

class App
{

    function __construct(){
        session_start();
    }

    public function login(){
        require('login.php');
    }

    public function auth(){
        echo "hola auth";
        if (isset($_REQUEST['user']) && ! empty($_REQUEST['user'])) {
            $user=$_REQUEST['user'];
            $_SESSION['user']=$user;
            header('Location:index.php?method=home');
        }else{
            header('Location:index.php?method=login');
        }
        return;
    }

    public function home(){
        if (!isset($_SESSION['user'])) {
            header('Location:index.php?method=login');
            return;
        }
        $user=$_SESSION['user'];

        if (isset($_SESSION['deseos'])) {
            $deseos=$_SESSION['deseos'];
        }else{
            $deseos=[];
        }
        require('list.php');
    }

    public function close(){ //cerrar sesion
        session_destroy();
        unset($_SESSION);
        header('Location:index.php?method=login');
    }

    public function new(){//CREAR DESEOS
        if (isset($_REQUEST['deseo']) && ! empty($_REQUEST['deseo'])){

            $_SESSION['deseos'][]=$_REQUEST['deseo'];
            // $deseos=$_SESSION['deseos'];
            // $deseos=$_REQUEST['deseo'];
            // $_SESSION['deseos']=$deseos;
        }
        header('Location:?method=home');
    }

    public function delete(){
        echo "Borrado deseo $_REQUEST[key]";
        $key=(integer) $_REQUEST['key'];
        unset($_SESSION['deseos'][$key]);
        header('Location:?method=home');
    }

    public function deleteAll(){
        unset($_SESSION['deseos']);
        header('Location:?method=home');
    }

}
