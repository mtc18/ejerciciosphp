<!DOCTYPE html>
<html>
<head>
    <title>sesiones varias</title>
</head>
<body>
    <h1>Lista de deseos (sesiones) de <?php echo isset($_SESSION['user']) ? $_SESSION['user'] : ' ' ?> </h1>

    <p><a href="?method=close">Cerrar Sesion</a></p>

    <form method="post" action="?method=new">
        <label>Deseos:</label>
        <input type="text" name="deseo" value="">
        <br>
        <input type="submit" name="submit"><br>
        <hr>
        <h1>Lista de Deseos: </h1>
        <ul>
            <?php foreach ($deseos as $clave=> $deseo): ?>
                <li><?php echo $deseo ?>
                    <a href="?method=delete&key=<?php echo $clave ?>">Borrar Deseo</a>
                </li>
            <?php endforeach?>
        </ul>
        <a href="?method=deleteAll">Borrar Lista</a>
    </body>
    </html>

